--      ████████╗ ██████╗ ██████╗     ██████╗  █████╗ ███╗   ██╗███████╗██╗
--      ╚══██╔══╝██╔═══██╗██╔══██╗    ██╔══██╗██╔══██╗████╗  ██║██╔════╝██║
--         ██║   ██║   ██║██████╔╝    ██████╔╝███████║██╔██╗ ██║█████╗  ██║
--         ██║   ██║   ██║██╔═══╝     ██╔═══╝ ██╔══██║██║╚██╗██║██╔══╝  ██║
--         ██║   ╚██████╔╝██║         ██║     ██║  ██║██║ ╚████║███████╗███████╗
--         ╚═╝    ╚═════╝ ╚═╝         ╚═╝     ╚═╝  ╚═╝╚═╝  ╚═══╝╚══════╝╚══════╝

-- ===================================================================
-- Initialization
-- ===================================================================


local awful = require("awful")
local beautiful = require("beautiful")
local wibox = require("wibox")
local gears = require("gears")
local dpi = beautiful.xresources.apply_dpi

-- import widgets
local task_list = require("widgets.task-list")

local volume_widget = require('awesome-wm-widgets.volume-widget.volume')

-- define module table
local top_panel = {}



-- ===================================================================
-- Bar Creation
-- ===================================================================


top_panel.create = function(s)
   local panel = awful.wibar({
      screen = s,
      position = "top",
      ontop = true,
      height = beautiful.top_panel_height,
      width = s.geometry.width,
   })

   panel:setup {
      expand = "none",
      layout = wibox.layout.align.horizontal,

      -- LISTA DE CLIENTES (VENTANAS) EN ESQUINA IZQUIERDA
      task_list.create(s),

      -- CALENDARIO EN MEDIO
      require("widgets.calendar").create(s),

      {
         -- ESTOS SON LOS ÍCONOS EN LA ESQUINA SUPERIOR DERECHA
        layout = wibox.layout.fixed.horizontal,
        wibox.layout.margin(wibox.widget.systray(), dpi(1), dpi(1), dpi(1), dpi(1)),
        --require("widgets.bluetooth"),
        --require("widgets.network")(),
        --require("widgets.battery"),

        -- CPU
        require("awesome-wm-widgets.cpu-widget.cpu-widget"){
           width = 70,
           step_width = 2,
           step_spacing = 0,
           color = '#434c5e'
        },

        -- RAM
        require("awesome-wm-widgets.ram-widget.ram-widget"){
           color_used = "#dd3c65",
           color_free = "#54ff54",
           color_buf = "#1fa1e0",
           timeout = 3
        },

        -- Batería
		--require("awesome-wm-widgets.battery-widget.battery"){
			--path_to_icons = os.getenv("HOME") .. "/.config/awesome/icons/battery/"
		--},

        -- Brillo
        --require("awesome-wm-widgets.brightness-widget.brightness"){
           --type = 'icon_and_text',
           --program = 'xbacklight',
           --step = 2,
        --},

        -- Volumen
        volume_widget{ widget_type = 'icon_and_text' },

        wibox.layout.margin(require("widgets.layout-box"), dpi(1), dpi(1), dpi(1), dpi(1))
      }
   }


   -- ===================================================================
   -- Functionality
   -- ===================================================================


   -- hide panel when client is fullscreen
   local function change_panel_visibility(client)
      if client.screen == s then
         panel.ontop = not client.fullscreen
      end
   end

   -- connect panel visibility function to relevant signals
   client.connect_signal("property::fullscreen", change_panel_visibility)
   client.connect_signal("focus", change_panel_visibility)

end

-- Teclas de widgets
--top_panel.keys = gears.table.join(
	--awful.key({}, "XF86AudioRaiseVolume", function() volume_widget:inc(5) end, {}),
	--awful.key({}, "XF86AudioLowerVolume", function() volume_widget:dec(5) end, {}),
	--awful.key({}, "XF86AudioMute", function() volume_widget:toggle() end, {})
--)


return top_panel
