--      ██╗  ██╗███████╗██╗   ██╗███████╗
--      ██║ ██╔╝██╔════╝╚██╗ ██╔╝██╔════╝
--      █████╔╝ █████╗   ╚████╔╝ ███████╗
--      ██╔═██╗ ██╔══╝    ╚██╔╝  ╚════██║
--      ██║  ██╗███████╗   ██║   ███████║
--      ╚═╝  ╚═╝╚══════╝   ╚═╝   ╚══════╝


-- ===================================================================
-- Initialization
-- ===================================================================


local awful = require("awful")
local gears = require("gears")
local naughty = require("naughty")
local beautiful = require("beautiful")
local xrandr = require("components.xrandr")
local dpi = beautiful.xresources.apply_dpi

-- Define mod keys
local modkey = "Mod4"
local altkey = "Mod1"

-- define module table
local keys = {}

-- Nota: no puedo mapear {modkey} + t, por algun motivo


-- ===================================================================
-- Movement Functions (Called by some keybinds)
-- ===================================================================


-- Move given client to given direction
local function move_client(c, direction)
   -- If client is floating, move to edge
   if c.floating or (awful.layout.get(mouse.screen) == awful.layout.suit.floating) then
      local workarea = awful.screen.focused().workarea
      if direction == "up" then
         c:geometry({nil, y = workarea.y + beautiful.useless_gap * 2, nil, nil})
      elseif direction == "down" then
         c:geometry({nil, y = workarea.height + workarea.y - c:geometry().height - beautiful.useless_gap * 2 - beautiful.border_width * 2, nil, nil})
      elseif direction == "left" then
         c:geometry({x = workarea.x + beautiful.useless_gap * 2, nil, nil, nil})
      elseif direction == "right" then
         c:geometry({x = workarea.width + workarea.x - c:geometry().width - beautiful.useless_gap * 2 - beautiful.border_width * 2, nil, nil, nil})
      end
   -- Otherwise swap the client in the tiled layout
   elseif awful.layout.get(mouse.screen) == awful.layout.suit.max then
      if direction == "up" or direction == "left" then
         awful.client.swap.byidx(-1, c)
      elseif direction == "down" or direction == "right" then
         awful.client.swap.byidx(1, c)
      end
   else
      awful.client.swap.bydirection(direction, c, nil)
   end
end


-- Redimensionar cliente en cierta direccion
local floating_resize_amount = dpi(20)
local tiling_resize_factor = 0.01

local function resize_client(c, direction)
   if awful.layout.get(mouse.screen) == awful.layout.suit.floating or (c and c.floating) then
      if direction == "up" then
         c:relative_move(0, 0, 0, -floating_resize_amount)
      elseif direction == "down" then
         c:relative_move(0, 0, 0, floating_resize_amount)
      elseif direction == "left" then
         c:relative_move(0, 0, -floating_resize_amount, 0)
      elseif direction == "right" then
         c:relative_move(0, 0, floating_resize_amount, 0)
      end
   else
      if direction == "up" then
         awful.client.incwfact(-tiling_resize_factor)
      elseif direction == "down" then
         awful.client.incwfact(tiling_resize_factor)
      elseif direction == "left" then
         awful.tag.incmwfact(-tiling_resize_factor)
      elseif direction == "right" then
         awful.tag.incmwfact(tiling_resize_factor)
      end
   end
end


-- raise focused client
local function raise_client()
   if client.focus then
      client.focus:raise()
   end
end


-- ===================================================================
-- Mouse bindings
-- ===================================================================


-- Mouse buttons on the desktop
keys.desktopbuttons = gears.table.join(
   -- left click on desktop to hide notification
   awful.button({}, 1,
      function ()
         naughty.destroy_all_notifications()
      end
   )
)

-- Mouse buttons on the client
keys.clientbuttons = gears.table.join(
   -- Raise client
   awful.button({}, 1,
      function(c)
         client.focus = c
         c:raise()
      end
   ),

   -- Mover y redimensionar cliente
   awful.button({modkey}, 1, awful.mouse.client.move),
   awful.button({modkey}, 3, awful.mouse.client.resize)
)

-- Disable floating window snapping
-- awful.mouse.snap.edge_enable = false


-- ===================================================================
-- Desktop Key bindings
-- ===================================================================

keys.globalkeys = gears.table.join(
   -- =========================================
   -- SPAWN APPLICATION KEY BINDINGS
   -- =========================================

   -- Abrir terminal

   --No uso esto, porque lo pongo en sxhkd
   --[[
      [awful.key({modkey}, "Return",
      [   function()
      [      awful.spawn(apps.terminal)
      [   end,
      [   {description = "open a terminal", group = "launcher"}
      [),
      ]]
   -- launch rofi
--[[
   [   awful.key({modkey}, "d",
   [      function()
   [         awful.spawn(apps.launcher)
   [      end,
   [      {description = "application launcher", group = "launcher"}
   [   ),
   ]]

   -- =========================================
   -- FUNCTION KEYS
   -- =========================================

   -- Brillo
   -- La implementación actual del widget de brillo consume
   -- mucho CPU cuando se deja presionada la tecla, por eso no la uso

   --[[
      [awful.key({modkey}, "XF86MonBrightnessUp",
      [   function()
      [      awful.spawn("xbacklight -inc 5", false)
      [      awesome.emit_signal("brightness_change")
      [   end,
      [   {description = "+5%", group = "hotkeys"}
      [),
      [awful.key({modkey}, "XF86MonBrightnessDown",
      [   function()
      [      awful.spawn("xbacklight -dec 5", false)
      [      awesome.emit_signal("brightness_change")
      [   end,
      [   {description = "-5%", group = "hotkeys"}
      [),
      [awful.key({}, "XF86MonBrightnessUp",
      [   function()
      [      awful.spawn("xbacklight -inc 1", false)
      [      awesome.emit_signal("brightness_change")
      [   end,
      [   {description = "+1%", group = "hotkeys"}
      [),
      [awful.key({}, "XF86MonBrightnessDown",
      [   function()
      [      awful.spawn("xbacklight -dec 1", false)
      [      awesome.emit_signal("brightness_change")
      [   end,
      [   {description = "-1%", group = "hotkeys"}
      [),
	  ]]

   -- ALSA control de volumen

   awful.key({}, "XF86AudioRaiseVolume",
      function()
         awful.spawn("amixer -q -D pulse sset Master 5%+", false)
         awesome.emit_signal("volume_change")
      end,
      {description = "volume up", group = "hotkeys"}
   ),
   awful.key({}, "XF86AudioLowerVolume",
      function()
         awful.spawn("amixer -q -D pulse sset Master 5%-", false)
         awesome.emit_signal("volume_change")
      end,
      {description = "volume down", group = "hotkeys"}
   ),
   awful.key({}, "XF86AudioMute",
      function()
         awful.spawn("amixer -D pulse set Master 1+ toggle", false)
         awesome.emit_signal("volume_change")
      end,
      {description = "toggle mute", group = "hotkeys"}
   ),

   -- No funcionan al recargar awesome/xmodmap?, poner en sxhkd
   --[[
      [awful.key({}, "XF86AudioRecord",
      [   function()
      [      awful.spawn("mpvs play-pause", false)
      [   end,
      [   {description = "siguiente música", group = "hotkeys"}
      [),
      [awful.key({}, "XF86AudioPrev",
      [   function()
      [      awful.spawn("mpvs previous", false)
      [   end,
      [   {description = "anterior música", group = "hotkeys"}
      [),
      [awful.key({}, "XF86AudioNext",
      [   function()
      [      awful.spawn("mpvs next", false)
      [   end,
      [   {description = "reproducir/pausar música", group = "hotkeys"}
      [),
	  ]]

   -- Screenshots
   --No uso esto, porque lo pongo en sxhkd
   --awful.key({}, "Print",
      --function()
         --awful.spawn(apps.screenshot, false)
      --end
   --),

   -- =========================================
   -- RECARGAR / SALIR DE AWESOME
   -- =========================================

   -- Recargar awesome
   awful.key({modkey, "Shift"}, "r",
      awesome.restart,
      {description = "recargar awesome", group = "awesome"}
   ),

   -- Pantalla de salida
   awful.key({modkey}, "Escape",
      function()
         -- emit signal to show the exit screen
         awesome.emit_signal("show_exit_screen")
      end,
      {description = "alternar pantalla de salida", group = "hotkeys"}
   ),

   awful.key({}, "XF86PowerOff",
      function()
         -- emit signal to show the exit screen
         awesome.emit_signal("show_exit_screen")
      end,
      {description = "alternar pantalla de salida", group = "hotkeys"}
   ),

   -- =========================================
   -- ENFOCADO DE CLIENTES
   -- =========================================

   -- Enfocar cliente por direccion (teclas jklñ)
   awful.key({modkey}, "l",
      function()
         awful.client.focus.bydirection("down")
         raise_client()
      end,
      {description = "focus down", group = "client"}
   ),
   awful.key({modkey}, "k",
      function()
         awful.client.focus.bydirection("up")
         raise_client()
      end,
      {description = "focus up", group = "client"}
   ),
   awful.key({modkey}, "j",
      function()
         awful.client.focus.bydirection("left")
         raise_client()
      end,
      {description = "focus left", group = "client"}
   ),
   awful.key({modkey}, "ñ",
      function()
         awful.client.focus.bydirection("right")
         raise_client()
      end,
      {description = "focus right", group = "client"}
   ),

   -- Enfocar cliente por direccion (con flechas)
   awful.key({modkey}, "Down",
      function()
         awful.client.focus.bydirection("down")
         raise_client()
      end,
      {description = "enfocar abajo", group = "client"}
   ),
   awful.key({modkey}, "Up",
      function()
         awful.client.focus.bydirection("up")
         raise_client()
      end,
      {description = "enfocar arriba", group = "client"}
   ),
   awful.key({modkey}, "Left",
      function()
         awful.client.focus.bydirection("left")
         raise_client()
      end,
      {description = "enfocar izquierda", group = "client"}
   ),
   awful.key({modkey}, "Right",
      function()
         awful.client.focus.bydirection("right")
         raise_client()
      end,
      {description = "enfocar derecha", group = "client"}
   ),

   -- Enfocar cliente por indice (Ciclar entre clientes)
   awful.key({modkey}, "Tab",
      function()
         awful.client.focus.byidx(1)
      end,
      {description = "enfocar siguiente por índice", group = "client"}
   ),
   awful.key({modkey, "Shift"}, "Tab",
      function()
         awful.client.focus.byidx(-1)
      end,
      {description = "enfocar anterior por índice", group = "client"}
   ),

   -- =========================================
   -- ENFOCADO DE PANTALLAS
   -- =========================================

   -- Enfocar en pantallas/monitores por su indice
   awful.key({modkey}, "s",
      function()
         awful.screen.focus_relative(1)
      end
   ),

   -- =========================================
   -- DISPOSICION DE PANTALLAS
   -- =========================================

   -- Abrir una notificacion que cicla diferentes disposiciones de monitores
   awful.key({modkey, "Shift"}, "s",
      function()
          xrandr.xrandr()
      end
   ),

   -- =========================================
   -- REDIMENSIONAR CLIENTES
   -- =========================================
   -- Nota: modkey + clic derecho: Redimensionar → awful.mouse.client.resize

   awful.key({modkey, "Shift"}, "Down",
      function(c)
         resize_client(client.focus, "down")
      end
   ),
   awful.key({modkey, "Shift"}, "Up",
      function(c)
         resize_client(client.focus, "up")
      end
   ),
   awful.key({modkey, "Shift"}, "Left",
      function(c)
         resize_client(client.focus, "left")
      end
   ),
   awful.key({modkey, "Shift"}, "Right",
      function(c)
         resize_client(client.focus, "right")
      end
   ),
   awful.key({modkey, "Shift"}, "l",
      function(c)
         resize_client(client.focus, "down")
      end
   ),
   awful.key({ modkey, "Shift" }, "k",
      function(c)
         resize_client(client.focus, "up")
      end
   ),
   awful.key({modkey, "Shift"}, "j",
      function(c)
         resize_client(client.focus, "left")
      end
   ),
   awful.key({modkey, "Shift"}, "ñ",
      function(c)
         resize_client(client.focus, "right")
      end
   ),

   -- =========================================
   -- Clientes maestros / Clientes columna
   -- =========================================

   -- Número de clientes maestros
   awful.key({modkey, altkey}, "j",
      function()
         awful.tag.incnmaster( 1, nil, true)
      end,
      {description = "incrementar número de clientes maestros", group = "layout"}
   ),
   awful.key({ modkey, altkey }, "ñ",
      function()
         awful.tag.incnmaster(-1, nil, true)
      end,
      {description = "reducir número de clientes maestros", group = "layout"}
   ),
   awful.key({ modkey, altkey }, "Left",
      function()
         awful.tag.incnmaster( 1, nil, true)
      end,
      {description = "incrementar número de clientes maestros", group = "layout"}
   ),
   awful.key({ modkey, altkey }, "Right",
      function()
         awful.tag.incnmaster(-1, nil, true)
      end,
      {description = "reducir número de clientes maestros", group = "layout"}
   ),

   -- Número de columnas (clientes verticales)
   awful.key({modkey, altkey}, "k",
      function()
         awful.tag.incncol(1, nil, true)
      end,
      {description = "incrementar número de columnas", group = "layout"}
   ),
   awful.key({modkey, altkey}, "l",
      function()
         awful.tag.incncol(-1, nil, true)
      end,
      {description = "reducir número de columnas", group = "layout"}
   ),
   awful.key({modkey, altkey}, "Up",
      function()
         awful.tag.incncol(1, nil, true)
      end,
      {description = "increase the number of columns", group = "layout"}
   ),
   awful.key({modkey, altkey}, "Down",
      function()
         awful.tag.incncol(-1, nil, true)
      end,
      {description = "reducir número de columnas", group = "layout"}
   ),

   -- =========================================
   -- CONTROL DE HUECOS
   -- =========================================

   -- Control de huecos
   awful.key({modkey, "Shift"}, "minus",
      function()
         awful.tag.incgap(2, nil)
      end,
      {description = "incrementar tamaño de huecos para etiqueta actual", group = "gaps"}
   ),
   awful.key({modkey}, "minus",
      function()
         awful.tag.incgap(-2, nil)
      end,
      {description = "reducir tamaño de huecos para etiqueta actual", group = "gaps"}
   ),

   -- =========================================
   -- Seleccion de layout
   -- =========================================

   -- Seleccionar siguiente layout
   awful.key({modkey}, "space",
      function()
         awful.layout.inc(1)
      end,
      {description = "siguiente layout", group = "layout"}
   ),
   -- Seleccionar layout previo
   awful.key({modkey, "Shift"}, "space",
      function()
         awful.layout.inc(-1)
      end,
      {description = "anterior layout", group = "layout"}
   ),

   -- =========================================
   -- Minimizacion de clientes
   -- =========================================

   -- Restaurar cliente minimizado
   awful.key({modkey, "Shift"}, "n",
      function()
         local c = awful.client.restore()
         -- Enfocar cliente restaurado
         if c then
            client.focus = c
            c:raise()
         end
      end,
      {description = "des minimizar", group = "client"}
   )
)


-- Función para depuración
--local function log(file,string, mode)
	--local mode = mode or 'w'
	--local fh = io.open(file, mode)
	--fh:write(string)
	--fh:close()
--end

--package.path = package.path..';'..os.getenv('HOME')..'/.luarocks/share/lua/5.4/?.lua;'..os.getenv('HOME')..'/.luarocks/share/lua/5.4/?/init.lua'
--require 'pl'
--fh:write(pretty.write(keys.globalkeys))

-- Agregar teclas de paneles
--local top_keys = require("components.pastel.top-panel").keys
--for _, e in ipairs(top_keys) do
	--table.insert( keys.globalkeys, e)
--end

-- ===================================================================
-- Control de cliente
-- ===================================================================


keys.clientkeys = gears.table.join(
   -- Intercambiar clientes o moverlos al costado
   awful.key({modkey, "Control"}, "Down",
      function(c)
         move_client(c, "down")
      end
   ),
   awful.key({modkey, "Control"}, "Up",
      function(c)
         move_client(c, "up")
      end
   ),
   awful.key({modkey, "Control"}, "Left",
      function(c)
         move_client(c, "left")
      end
   ),
   awful.key({modkey, "Control"}, "Right",
      function(c)
         move_client(c, "right")
      end
   ),
   awful.key({modkey, "Control"}, "l",
      function(c)
         move_client(c, "down")
      end
   ),
   awful.key({modkey, "Control"}, "k",
      function(c)
         move_client(c, "up")
      end
   ),
   awful.key({modkey, "Control"}, "j",
      function(c)
         move_client(c, "left")
      end
   ),
   awful.key({modkey, "Control"}, "ñ",
      function(c)
         move_client(c, "right")
      end
   ),

   -- Alternar pantalla completa
   awful.key({modkey}, "f",
      function(c)
         c.fullscreen = not c.fullscreen
      end,
      {description = "alternar pantalla completa", group = "client"}
   ),

   -- Cerrar cliente
   -- No uso esto, pues lo pongo en sxhkd
--[[
   [   awful.key({modkey}, "q",
   [      function(c)
   [         c:kill()
   [      end,
   [      {description = "close", group = "client"}
   [   ),
   [
   ]]
   -- Alternar flotado
   -- Nota: modkey + clic izquierdo: agarrar cliente flotante → awful.mouse.client.move
   awful.key({modkey}, "a",
      awful.client.floating.toggle,
      {description = "alternar flotado", group = "client"}
   ),

   -- Alternar permanecer arriba  -- keep on top
   awful.key({modkey}, "p",
      function(c)
        c.ontop = not c.ontop
      end,
      {description = "alternar permanencia arriba", group = "client"}
   ),

   -- Alternar permanecer abajo  -- keep below
   awful.key({modkey, "Shift"}, "p",
      function(c)
        c.below = not c.below
      end,
      {description = "alternar permanencia abajo", group = "client"}
   ),

   -- Minimizar
   awful.key({modkey}, "n",
      function(c)
         c.minimized = true
      end,
      {description = "minimizar", group = "client"}
   ),

   -- Maximizar
   awful.key({modkey}, "m",
      function(c)
         c.maximized = not c.maximized
         c:raise()
      end,
      {description = "alternar maximizado", group = "client"}
   ),

   -- Cliente en todas las etiquetas
   awful.key({modkey}, "o",
      function(c)
         c.sticky = not c.sticky
      end,
      {description = "sticky", group = "client"}
   )
)

-- Bind all key numbers to tags
for i = 1, 9 do
   keys.globalkeys = gears.table.join(keys.globalkeys,
      -- Ir a etiqueta
      awful.key({modkey}, "#" .. i + 9,
         function()
            local screen = awful.screen.focused()
            local tag = screen.tags[i]
            if tag then
               tag:view_only()
            end
         end,
         {description = "ir a etiqueta #"..i, group = "tag"}
      ),
      -- Mover cliente a etiqueta
      awful.key({modkey, "Shift"}, "#" .. i + 9,
         function()
            if client.focus then
               local tag = client.focus.screen.tags[i]
               if tag then
                  client.focus:move_to_tag(tag)
               end
            end
         end,
         {description = "mover cliente enfocado a etiqueta #"..i, group = "tag"}
      ),
      -- Alternar mostrar etiqueta
      awful.key({modkey, "Control"}, "#" .. i + 9,
         function()
             local screen = awful.screen.focused()
             local tag = screen.tags[i]
             if tag then
                 awful.tag.viewtoggle(tag)
             end
         end,
         {description = "alternar etiqueta #"..i, group = "tag"}
      )
   )
   --[[
      [   -- Alternar mostrar cliente en etiqueta
      [   awful.key({modkey, "Control"}, "#" .. i + 9,
      [      function()
      [          if client.focus then
      [              local tag = client.focus.screen.tags[i]
      [              if tag then
      [                  client.focus:toggle_tag(tag)
      [              end
      [         end
      [      end,
      [      {description = "Toggle tag #"..i, group = "tag"}
      [   )
      [)
      ]]
end

return keys
