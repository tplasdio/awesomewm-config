#!/usr/bin/lua
local wibox = require("wibox")
local watch = require("awful.widget.watch")

local text = wibox.widget{
	font = "FreeSans Bold 13",
	--margin = 20,
	--spacing = 20,
	widget = wibox.widget.textbox,
}

local battery_widget = wibox.widget.background()

battery_widget:set_widget(text)
battery_widget:set_fg("#ffffff")

watch(
	"bateria", 1,
	function(_, stdout, stderr, exitreason, exitcode)

		text:set_text(stdout)
		percentage = tonumber(string.match(stdout, "[0-9]+"))
		if (percentage > 50) then
			battery_widget:set_bg("#008800")
			battery_widget:set_fg("#ffffff")
		elseif (percentage > 18) then
			battery_widget:set_bg("#AB7300")
			battery_widget:set_fg("#ffffff")
		else
			battery_widget:set_bg("#880000")
			battery_widget:set_fg("#ffffff")
		end

		collectgarbage()

	end,
	battery_widget
)

text:set_text("???")

return battery_widget
